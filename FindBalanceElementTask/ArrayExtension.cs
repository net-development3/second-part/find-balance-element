﻿using System;

namespace FindBalanceElementTask
{
    public static class ArrayExtension
    {
        public static int? FindBalanceElement(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException(null, nameof(array));
            }

            int? result = null;

            for (int i = 1; i <= array.Length - 1; i++)
            {
                long leftSum = 0, rightSum = 0;
                bool flagLeft = false, flagRight = false;

                for (int l = 0; l < i; l++)
                {
                    leftSum += array[l];
                    flagLeft = true;
                }
                
                for (int r = array.Length - 1; r > i; r--)
                {
                    rightSum += array[r];
                    flagRight = true;
                }

                if (leftSum == rightSum && flagLeft && flagRight)
                {
                    result = i;
                    break;
                }
            }

            return result;
        }
    }
}
